const rates = {
  M: 1000,
  CM: 900,
  D: 500,
  CD: 400,
  C: 100,
  XC: 90,
  L: 50,
  XL: 40,
  X: 10,
  IX: 9,
  V: 5,
  IV: 4,
  I: 1,
};
const regexEx = /(CM)|(CD)|(XC)|(XL)|(IX)|(IV)/gim;

const arabic = (number) => {
  let arabicNumber = 0;
  let result;
  if (regexEx.test(number)) {
    const exceptions = number.match(regexEx);
    const cleanRoman = number.replace(regexEx, '');
    result = [...exceptions, ...cleanRoman.split('')];
  } else {
    result = number.split('');
  }
  result.forEach((item) => {
    arabicNumber += Number(rates[item]);
  });
  return arabicNumber;
};

arabic('MMMMMMMI');
arabic('MMMMMMMXLI');

export default arabic;
