const st = 'hello-there';
// substring
const subst1 = st.substring(6);
const subst2 = st.substring(6, 9);
const subst3 = st.substring(6, -2);
console.log(subst1, subst2, subst3);
// slice
const sl1 = st.slice(6);
const sl2 = st.slice(6, 9);
const sl3 = st.slice(6, -2);
console.log(sl1, sl2, sl3);
