const fetchGithub = async (user, page) => {
  const json = await fetch(
    `https://api.github.com/users/${user}/repos?page=${page}`
  );
  const response = await json.json();

  return response;
};

// fetchGithub('tarcea', 2).then((a) => {
//   console.log(a.length);
// });

let stop = false;

const one = setInterval(() => {
  stop = true;
}, 500);

const two = setInterval(() => {
  clearInterval(one);
  // console.log('after', stop);
}, 555);

for (;;) {
  console.log(stop);
  // if (stop) {
  //   break;
  // } else {
  //   clearInterval(two);
  //   console.log('sase');
  // }
}
