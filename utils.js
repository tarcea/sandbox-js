// display every n seconds
const rollSkills = (skillsArray) => {
  skillsArray.push('My Skills');
  skillsArray.unshift('My Skills');
  for (let i = 0; i < skillsArray.length; i++) {
    ((i) => {
      setTimeout(() => {
        console.log(skillsArray[i]);
      }, 3000 * i);
    })(i);
  }
};

const skillsArray = ['Java Script', 'Type Script', 'Postgresql'];

rollSkills(skillsArray);
