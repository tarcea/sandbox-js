const outer = () => {
  let counter = 0;
  const incrementCounter = () => {
    counter++;
    return counter;
  };

  return incrementCounter;
};

const myNewFunction = outer();
const a = myNewFunction();
const b = myNewFunction();
const c = myNewFunction();
const d = myNewFunction();
const e = myNewFunction();
console.log(a, b, c, d, e);
