const toRomans = (number) => {
  const input = number;
  const string = `${input}`;
  const { length } = string;
  let result;
  const thousands = 'M'.repeat(Math.floor(input / 1000));

  const onesArray = [
    '',
    'I',
    'II',
    'III',
    'IV',
    'V',
    'VI',
    'VII',
    'VIII',
    'IX',
  ];
  const tensArray = [
    '',
    'X',
    'XX',
    'XXX',
    'XL',
    'L',
    'LX',
    'LXX',
    'LXXX',
    'XC',
  ];
  const hundredsArray = [
    '',
    'C',
    'CC',
    'CCC',
    'CD',
    'D',
    'DC',
    'DCC',
    'DCCC',
    'CM',
  ];
  const hundreds = hundredsArray[string[length - 3]];
  const tens = tensArray[string[length - 2]];
  const ones = onesArray[string[length - 1]];

  switch (length) {
    case 1:
      result = onesArray[string[0]];
      break;
    case 2:
      result = tensArray[string[0]] + onesArray[string[1]];
      break;
    case 3:
      result =
        hundredsArray[string[0]] + tensArray[string[1]] + onesArray[string[2]];
      break;
    default:
      result = thousands + hundreds + tens + ones;
  }
  console.log(result);
  return result;
};
export default toRomans;
