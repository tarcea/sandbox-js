const outside = 'Outside';
function makeFunc() {
  const name = 'Closure';
  function displayName() {
    console.log(outside + ' ' + name);
  }
  return displayName;
}

const myFunc = makeFunc();
myFunc();
